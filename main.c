#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <windows.h>

int main()
{
    int folder_tmp_number=-1;
    short patient_photo_counter=0;
    short patient_number_counter=0;
    char command[100];
    char temp_num[100];

    DIR *dp;
    struct dirent *ep;
    dp = opendir ("./");
    if (dp != NULL)
    {
        //create initial folder
        strcpy(command, "mkdir ");
        strcat(command, itoa(folder_tmp_number, temp_num, 10));
        system(command);

        while (ep = readdir (dp))
        {
            char filename[100];
            strcpy(filename, ep->d_name);

            // if is a jpg picture file
            if (strstr(filename, ".jpg") != NULL || strstr(filename, ".JPG") != NULL)
            {
                // if is not a delimiter file
                if (strstr(filename, "#") == NULL)
                {
                    puts(filename);
                    strcpy(command, "copy ");
                    strcat(command, "\"");
                    strcat(command, filename);
                    strcat(command, "\"");
                    strcat(command, " ");
                    strcat(command, itoa(folder_tmp_number, temp_num, 10));
                    strcat(command, " >NUL");
                    system(command);
                    patient_photo_counter++;
                }
                else
                {

                    /* As reqeusted, also copy file if it's a delimeter file*/

                    puts(filename);
                    strcpy(command, "copy ");
                    strcat(command, "\"");
                    strcat(command, filename);
                    strcat(command, "\"");
                    strcat(command, " ");
                    strcat(command, itoa(folder_tmp_number, temp_num, 10));
                    strcat(command, " >NUL");
                    system(command);
                    patient_photo_counter++;

                    // get classification name and folder name
                    strtok(filename, "#");
                    char classification_folder_name[100];
                    strcpy(classification_folder_name, strtok(NULL, "#"));
                    char folder_name[100];
                    strcpy(folder_name, strtok(strtok(NULL, "#"), "."));

                    // create classification\foldername folder
                    strcpy(command, "mkdir ");
                    strcat(command, classification_folder_name);
                    strcat(command, "\\");
                    strcat(command, folder_name);
                    system(command);

                    // copy the content
                    strcpy(command, "xcopy ");
                    strcat(command, itoa(folder_tmp_number, temp_num, 10));
                    strcat(command, " ");
                    strcat(command, classification_folder_name);
                    strcat(command, "\\");
                    strcat(command, folder_name);
                    strcat(command, " /E /Y");
                    system(command);

                    Sleep(500);

                    // remove temporary numbered folder
                    strcpy(command, "rd ");
                    strcat(command, itoa(folder_tmp_number, temp_num, 10));
                    strcat(command, " /S /Q");
                    system(command);

                    printf("%d files copied\n", patient_photo_counter);

                    // clear picture counter, increment patient counter
                    folder_tmp_number--;
                    patient_photo_counter = 0;
                    patient_number_counter++;

                    // create temporary numbered folder for next patient
                    strcpy(command, "mkdir ");
                    strcat(command, itoa(folder_tmp_number, temp_num, 10));
                    system(command);

                    puts("--------------------------------------");
                }
            }
        }
        (void) closedir (dp);
    }
    else
        perror ("Couldn't open the directory");

    // remove redundant temporary numbered folder
    strcpy(command, "rd ");
    strcat(command, itoa(folder_tmp_number, temp_num, 10));
    system(command);

    printf("%d patients processed\n", patient_number_counter);

    return 0;
}
