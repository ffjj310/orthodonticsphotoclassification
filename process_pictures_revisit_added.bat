@echo off 
setlocal enabledelayedexpansion

:: PREREQUISITE: all files must be strictly separable by name card photo in filename order
:: USAGE: rename name card photo as "NameCardPhotoFilename#ExtraInformation"
::        do NOT include "#" in the extra information or name card photo filename 
::        because it's been set as the delimiter (at line 25 and 32)
::        if you must include "#" in the extra information or card photo filename, modify line 25 and 32

:: My basic scheme for this script is:
:: create a temporary new folder named by increasing number
:: copy the pictures untill meet name card photo
:: rename the temporary folder by the name card information provided

:: increasing number for temporary folders
set num=1
call :SUB_CreateFolder "!num!"
:: counter for each patient's photos
set counter=0

:: iterate through files in name order
for /f "tokens=*" %%f in ('dir /b *.jpg ^| sort') do (
	set name=%%f
	:: if it's a normal picture
	if !name!==!name:#=! (
		echo !name!
		copy /Y !name! !num! >NUL
		set /A counter=!counter!+1
	) else (
	:: if it's a name card picture
		echo !counter! new files copied
		set extrainfo=!name:*###=!
		set extrainfo=!extrainfo:.jpg=!
		call :SUB_RenameFolder "!extrainfo!"
		
		set classification=!name:*#=!
		set classification=!classification:~0,-14!
		set classification=!classification:#=!
		::echo !classification!
		call :SUB_CreateFolder "!classification!\!extrainfo!"
		xcopy !extrainfo! !classification!\!extrainfo! /E /Y
		rd !extrainfo! /S /Q
		
		set /A num=!num!+1
		call :SUB_CreateFolder "!num!"
		set counter=0
		echo ------------------------------
	)
)
:: remove the last redundant folder
rd !num!
set /A num=!num!-1
echo !num! patients processed
echo.
pause
GOTO :EOF

::
:: sub routines
::
:: check if exist before creating a folder
:SUB_CreateFolder
set foldername=%~1
if not exist !foldername! md !foldername!
exit /b

:: check if exist before renaming the folder
:SUB_RenameFolder
set extrainfo=%~1
echo !extrainfo!
if not exist !extrainfo! (
	rename !num! !extrainfo!
	echo "new patient, processed"
) else (
	echo "patient already exist, overwriting"
	copy !num!\*.jpg !extrainfo!
	rd /S /Q !num!
)
exit /b

:: end of script
:EOF